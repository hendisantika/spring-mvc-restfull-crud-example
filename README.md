# Spring MVC RESTFull CRUD Example

## Things To Do:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-mvc-restfull-crud-example.git`
2. Go to folder: `cd spring-mvc-restfull-crud-example`
3. Package the app: `mvn clean package`

## Build + Deploy + Run application
Use the following maven commands to build, deploy and run Tomcat server.

`mvn clean install`  (This command triggers war packaging)

`mvn tomcat7:run` (This command run embedded tomcat and deploy war file automatically)

In this example, we will use the [POSTMAN](https://www.getpostman.com/downloads/) tool (A Chrome App) or [Insomnia](https://insomnia.rest/download/) to test our RESTful web services.

GET request (retrieve all books) - http://localhost:8080/bookApp/book

![retrieve all books](img/all.png "retrieve all books")

GET request (retrieve books by ID)- http://localhost:8080/bookApp/book/1

![retrieve books by ID](img/getById.png "retrieve books by ID")

POST request (Add New Book) - http://localhost:8080/bookApp/bookApp/book

![Add New Book](img/add.png "Add New Book")

PUT request (Update Book Data) - http://localhost:8080/bookApp/book/2

![Update Book Data](img/update.png "Update Book Data")

DELETE request - http://localhost:8080/bookApp/book/1

![Delete Book](img/delete.png "Delete Book")

