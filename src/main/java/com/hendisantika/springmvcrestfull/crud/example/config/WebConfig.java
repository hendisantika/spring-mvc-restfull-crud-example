package com.hendisantika.springmvcrestfull.crud.example.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mvc-restfull-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/01/20
 * Time: 09.31
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.hendisantika.springmvcrestfull.crud.example.controller"})
public class WebConfig extends WebMvcConfigurerAdapter {

}
