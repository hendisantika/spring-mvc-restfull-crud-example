package com.hendisantika.springmvcrestfull.crud.example.service;

import com.hendisantika.springmvcrestfull.crud.example.model.Book;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mvc-restfull-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/01/20
 * Time: 09.29
 */
public interface BookService {
    long save(Book book);

    Book get(long id);

    List<Book> list();

    void update(long id, Book book);

    void delete(long id);
}
